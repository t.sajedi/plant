import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget shadow({
  required double h,
  required double w,
  required Color color,
}) {
  return Container(
    height: h,
    width: w,
    color: color,
    // margin: EdgeInsets.only(left: 12),
  );
}
