class DiagnosePlant {
  final String imageURL;
  final String title;

  DiagnosePlant({required this.imageURL, required this.title});
}
