import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/BottomBarCtrl.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/models/MyPlants.dart/Plant.dart';

void showImageAlert(BuildContext context) {
  BottomBarCtrl bottomBarCtrl = Get.find();
  // PlantsPageCtrl plantsPageCtrl = Get.put(PlantsPageCtrl());
  final PlantsPageCtrl plantsPageCtrl = Get.find();

  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      String? path;
      // return object of type Dialog
      return AlertDialog(
        title: new Text("لطفا عکس گیاه مورد نظر را انتخاب کنید"),
        // content: new Text("Alert Dialog body"),
        actions: <Widget>[
          OutlinedButton(
            onPressed: () async {
              log("camera");
              List<Media>? res = await ImagesPicker.openCamera(
                pickType: PickType.image,
                // quality: 0.5,
                cropOpt: CropOption(
                  aspectRatio: CropAspectRatio.custom,
                ),
                // maxTime: 60,
              );
              if (res != null) {
                print(res[0].path);
                plantsPageCtrl.plants.add(
                  Plant(
                    name: "موز",
                    date: DateTime.now(),
                    imagePath: res[0].thumbPath.toString(),
                    caption: "میوه ای زرد رنگ",
                    lighting: "تمامی زیر افتاب",
                    watering: "به واسطه ی باران",
                    
                  ),
                );

                // setState(() {
                path = res[0].thumbPath;
                // });
              }
              Navigator.pop(context);
            },
            child: Text("دوربین"),
          ),
          OutlinedButton(
            onPressed: () async {
              List<Media>? res = await ImagesPicker.pick(
                count: 1,
                pickType: PickType.image,
                language: Language.System,
                // maxSize: 500,
                cropOpt: CropOption(
                  aspectRatio: CropAspectRatio.custom,
                ),
              );
              if (res != null) {
                print(res.map((e) => e.path).toList());
                plantsPageCtrl.plants.add(
                  Plant(
                    name: "موز",
                    date: DateTime.now(),
                    imagePath: res[0].thumbPath.toString(),
                    caption: "میوه ای زرد رنگ",
                    lighting: "تمامی زیر افتاب",
                    watering: "به واسطه ی باران",

                  ),
                );

                // setState(() {
                path = res[0].thumbPath;
                // });
                // bool status = await ImagesPicker.saveImageToAlbum(File(res[0]?.path));
                // print(status);
              }
              Navigator.pop(context);
            },
            child: Text("گالری"),
          ),
          // Spacer()
        ],
      );
    },
  ).then((value) {
    bottomBarCtrl.changePage(context, 3);
    // Navigator.pop(context);
  });
}
