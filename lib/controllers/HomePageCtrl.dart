import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/models/HomePage/GetStarted.dart';
import 'package:plant/models/HomePage/HomePageGridBtn.dart';
import 'package:plant/models/HomePage/HotBooks.dart';
import 'package:plant/models/HomePage/PopularPlants.dart';
import 'package:plant/views/Books/BoocksPage.dart';
import 'package:plant/views/Diagnose/DiagnosePage.dart';
import 'package:plant/views/Experts/ExpertsPage.dart';
import 'package:plant/views/MyPlants/MyPlantsPage.dart';

class HomePageCtrl extends GetxController {
  // this list for first btns in gridView
  List btnsList = [
    HomePageGridBTN(
      icon: FontAwesomeIcons.stethoscope,
      title: "تشخیص",
      iconColor: (Colors.green[800])!,
      pageIndex: DiagnosePage(),
      // pageIndex: 1,
    ),
    HomePageGridBTN(
      pageIndex: Container(),
      iconColor: (Colors.blue[700])!,
      icon: FontAwesomeIcons.camera,
      title: "شناسایی",
    ),
    HomePageGridBTN(
      pageIndex: ExpertsPage(),
      iconColor: (Colors.blue[700])!,
      icon: FontAwesomeIcons.headset,
      title: "پشتیبانی",
    ),
    HomePageGridBTN(
      pageIndex: Container(),
      iconColor: (Colors.yellow[700])!,
      icon: FontAwesomeIcons.gem,
      title: "پرمیوم",
    ),
    HomePageGridBTN(
      pageIndex: MyPlantsPage(),
      iconColor: (Colors.green[800])!,
      icon: FontAwesomeIcons.pagelines,
      title: "باغ من",
    ),
    HomePageGridBTN(
      pageIndex: BooksPage(),
      iconColor: (Colors.green[800])!,
      icon: FontAwesomeIcons.book,
      title: "کتاب ها",
    ),
    HomePageGridBTN(
      pageIndex: Container(),
      icon: FontAwesomeIcons.cameraRetro,
      title: "تشخیص 360",
      iconColor: (Colors.green[800])!,
    ),
    HomePageGridBTN(
      pageIndex: Container(),
      iconColor: (Colors.green[800])!,
      icon: FontAwesomeIcons.userClock,
      title: "یادآور",
    ),
  ].obs;

  //  this list for hot books part
  List hotBooks = [
    HotBook(
      title: "گل هفته گل هفته",
      imageURL:
          "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/gettyimages-743744991-612x612-1610127940.jpg",
      subtitle: "انحصاری",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "مختصری از تاریخچه گیاهان آپارتمانی",
          subtitle: [],
        ),
        HotBooksContent(
          title: "نحوه انتخاب گیاه آپارتمانی مناسب",
          subtitle: [],
        ),
        HotBooksContent(
          title: "گالری",
          subtitle: [
            "گیاهان آب گرم",
            "سلطان قلب ها",
            "پای فیل",
            "سرخس دلتا مو",
            "گیاه برگ چروک",
          ],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title:
          " راهنمای کامل باغبانی راهنمای کامل باغبانی راهنمای کامل باغبانی راهنمای کامل باغبانی",
      imageURL:
          "https://images.unsplash.com/photo-1559563362-c667ba5f5480?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXw4MjE3MzMyfHxlbnwwfHx8fA%3D%3D&w=1000&q=80",
      subtitle: "انحصاری",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "هفته 1 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 2 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 3 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 4 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 5 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 6 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 7 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 8 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 9 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 10 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 11 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 12 : بادمجان",
          subtitle: [],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title: "گل هفته",
      imageURL:
          "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/gettyimages-743744991-612x612-1610127940.jpg",
      subtitle: "انحصاری",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "مختصری از تاریخچه گیاهان آپارتمانی",
          subtitle: [],
        ),
        HotBooksContent(
          title: "نحوه انتخاب گیاه آپارتمانی مناسب",
          subtitle: [],
        ),
        HotBooksContent(
          title: "گالری",
          subtitle: [
            "گیاهان آب گرم",
            "سلطان قلب ها",
            "پای فیل",
            "سرخس دلتا مو",
            "گیاه برگ چروک",
          ],
        ),
      ],
      price: "0",
    ),
    HotBook(
      title: "راهنمای کامل باغبانی",
      imageURL:
          "https://images.unsplash.com/photo-1559563362-c667ba5f5480?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxjb2xsZWN0aW9uLXBhZ2V8MXw4MjE3MzMyfHxlbnwwfHx8fA%3D%3D&w=1000&q=80",
      subtitle: "انحصاری",
      description:
          "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است",
      contents: [
        HotBooksContent(
          title: "هفته 1 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 2 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 3 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 4 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 5 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 6 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 7 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 8 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 9 : بادمجان",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 10 : موز",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 11 : خیار",
          subtitle: [],
        ),
        HotBooksContent(
          title: "هفته 12 : بادمجان",
          subtitle: [],
        ),
      ],
      price: "0",
    ),
  ].obs;

  //  this list for get Started part
  List getStarteds = [
    GetStartedModel(
      imageURL:
          "https://www.cnet.com/a/img/2JJTAqanK_pIbSrpA17VXXneGQY=/1200x630/2021/03/29/1c2386e1-53b8-4a19-803d-08abd7ffe77e/floom.jpg",
      title: "چگونه می توان گیاهان را به راحتی با این برنامه شناسایی کرد",
    ),
    GetStartedModel(
      imageURL:
          "https://smartcdn.prod.postmedia.digital/montrealgazette/wp-content/uploads/2016/10/montreal-has-its-own-official-flower-created-in-quebec-in-c.jpeg",
      title: "انواع ، تنوع و موارد دیگر",
    ),
    GetStartedModel(
      imageURL:
          "https://images.unsplash.com/photo-1612250997334-ce693b03a29a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHB1cnBsZSUyMGZpZWxkfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
      title: "دلایلی که باعث می شود برخی از گیاهان متفاوت به نظر برسند",
    ),
  ].obs;

  List popUlarPlants = [
    PopularPlantsModel(
      imageURL:
          "https://www.gardeningknowhow.com/wp-content/uploads/2017/10/zinc-veggie-400x300.jpg",
      title: "سبزیجات",
    ),
    PopularPlantsModel(
      imageURL:
          "https://images.all-free-download.com/images/graphiclarge/green_leaf_background_05_hd_pictures_169227.jpg",
      title: "گیاهان برگ",
    ),
    PopularPlantsModel(
      imageURL:
          "https://s1.1zoom.me/big0/581/Roses_Closeup_Wood_planks_Black_Bench_512107_1280x853.jpg",
      title: "گل ها",
    ),
    PopularPlantsModel(
      imageURL:
          "https://parade.com/wp-content/uploads/2021/07/healthiest-fruits.jpg",
      title: "میوه ها",
    ),
    PopularPlantsModel(
      imageURL:
          "https://images.unsplash.com/photo-1604762511431-6280a12cb835?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80",
      title: "ساکولنت",
    ),
    PopularPlantsModel(
      imageURL:
          "https://cdn-prod.medicalnewstoday.com/content/images/articles/322/322383/cannabis-leaf-on-wooden-background.jpg",
      title: "علفهای هرز",
    ),
    PopularPlantsModel(
      imageURL: "https://ak.picdn.net/shutterstock/videos/5365214/thumb/1.jpg",
      title: "درخت ها",
    ),
    PopularPlantsModel(
      imageURL:
          "https://4.bp.blogspot.com/-e_0bSaMNjjw/VwHwgjdw-tI/AAAAAAAAQW8/a_APtl-nI-sMQVyML2Fx-JERyukQsI5cA/s1600/1a%2Brempah9.jpg",
      title: "گیاهان سمی",
    ),
  ].obs;
}
