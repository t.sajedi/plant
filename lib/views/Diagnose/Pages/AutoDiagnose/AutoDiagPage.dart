import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jalali_calendar/jalali_calendar.dart';
import 'package:plant/controllers/Diagnose/AutoDiagnoseCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Diagnose/Pages/AutoDiagnose/components/Appbar.dart';
import 'package:plant/views/Diagnose/Pages/AutoDiagnose/components/Images.dart';

class AutoDiagnosePage extends StatelessWidget {
  AutoDiagCtrl autoDiagCtrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: Column(
        children: [
          appBarAutoDiag(context),
          Card(
            color: Colors.white,
            elevation: 4,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            margin: EdgeInsets.all(horizmargin),
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: horizmargin, vertical: verticmargin),
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(height: verticmargin),
                  Text(
                    "تشخیص خودکار مشکلات گیاهان",
                    textAlign: TextAlign.end,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: verticmargin * 0.8),
                  Text(
                    "لطفا عکس های واضحی از زوایای مختلف بیماری گیاه و خود گیاه بگیرید",
                    textAlign: TextAlign.end,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 22,
                      // fontWeight: FontWeight.w600,
                      color: Colors.black54,
                    ),
                  ),
                  SizedBox(height: verticmargin),
                  Obx(() {
                    return Text(
                      // ignore: invalid_use_of_protected_member
                      "اضافه کردن عکس ها" + " (${autoDiagCtrl.picsCount}/3)",
                      textAlign: TextAlign.end,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 22,
                        // fontWeight: FontWeight.w600,
                        color: Colors.black,
                      ),
                    );
                  }),
                  SizedBox(height: verticmargin * 2),
                  Container(
                    // color: Colors.red,
                    height: 352,
                    child: GridView.builder(
                      padding: EdgeInsets.zero,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 3,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      itemBuilder: (context, index) {
                        return InkWell(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onTap: () {
                            showAutoImageAler(context: context,index: index);
                            log(autoDiagCtrl.pics[index]["image"]);
                          },
                          child: Obx(() {
                            return cardImage(context, autoDiagCtrl.pics[index]);
                          }),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
