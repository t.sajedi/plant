import 'package:flutter/material.dart';
import 'package:plant/controllers/Static.dart';

Widget price(BuildContext context) {
  return Container(
    height: 120,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.symmetric(horizontal: horizmargin),
    padding: EdgeInsets.symmetric(horizontal: horizmargin),
    color: Colors.green.withOpacity(0.2),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          "قیمت عادی 30 هزار تومان",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 24,
            color: Colors.grey,
          ),
        ),
        Text(
          "قیمت ویژه 25 هزار تومان",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w600,
            color: Colors.green,
          ),
        ),
        Text(
          "برای کاربران ویژه 20 درصد تخفیف",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 20,
            color: Colors.grey,
          ),
        ),
      ],
    ),
  );
}
