import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BottomBarCtrl.dart';
import 'package:plant/views/Camera/CameraPage.dart';
import 'package:plant/views/Diagnose/DiagnosePage.dart';
import 'package:plant/views/Home/HomaPage.dart';
import 'package:plant/views/More/MorePage.dart';
import 'package:plant/views/MyPlants/MyPlantsPage.dart';

// ignore: must_be_immutable
class Body extends StatelessWidget {
  BottomBarCtrl bottomBarCtrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => IndexedStack(
        index: bottomBarCtrl.page.value,
        children: [
          HomePage(),
          DiagnosePage(),
          CameraPage(),
          MyPlantsPage(),
          MorePage(),
        ],
      ),
    );
  }
}
