import 'package:flutter/material.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/More/Components/ColumnedText.dart';

Widget appbar(BuildContext context) {
  return Container(
    height: 205,
    color: Colors.grey[200],
    child: Stack(
      children: [
        Column(
          children: [
            Container(
              height: 65,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: horizmargin),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff31a689), Color(0xff3ecb62)],
                ),
              ),
              child: Row(
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "بیشتر",
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 42,
                      color: Colors.white,
                    ),
                  ),
                  Icon(
                    Icons.settings,
                    color: Colors.white,
                    size: 42,
                  ),
                ],
              ),
            ),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("assets/backCurve.png"),
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  columnedText(num: "1", title: "باغ من"),
                  Spacer(),
                  Container(
                    height: 35,
                    width: 0.5,
                    color: Colors.white30,
                  ),
                  Spacer(),
                  columnedText(num: "1", title: "تاریخچه"),
                  Spacer(),
                ],
              ),
            ),
          ],
        ),

        // SizedBox(height: verticmargin,),
        Positioned(
          bottom: 0,
          child: Container(
            height: 85,
            width:
                MediaQuery.of(context).size.width - horizmargin - horizmargin,
            margin: EdgeInsets.only(right: horizmargin, left: horizmargin),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("assets/become.png"),
              ),
            ),
            padding: EdgeInsets.symmetric(
                horizontal: horizmargin, vertical: verticmargin * 0.6),
            child: Row(
              textDirection: TextDirection.rtl,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                      color: Colors.green[900], shape: BoxShape.circle),
                  child: Center(
                    child: Icon(
                      Icons.person_pin_circle_rounded,
                      color: Colors.yellow[600],
                      size: 46,
                    ),
                  ),
                ),
                SizedBox(width: horizmargin * 0.8),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "عضو ممتاز شوید",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                        height: 1,
                        fontSize: 24,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: verticmargin*0.4,),
                    Text(
                      "دسترسی کامل به همه ویژگی ها",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                        height: 1,
                        fontSize: 18,
                        color: Colors.green[900],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
