import 'package:get/get.dart';
import 'package:plant/models/Experts/ExpertsSlider.dart';

class ExpertCtrl extends GetxController {
  RxInt currentPage = 0.obs;
  RxList msages = [
    ExpertsSlider(
      img: "assets/doc.png",
      txt: "از باغبانان اصلی ما مشاوره باغبانی بگیرید",
    ),
    ExpertsSlider(
      img: "assets/book.png",
      txt: "دایره المعارف گیاهی بیش از 10 هزار گونه",
    ),
    ExpertsSlider(
      img: "assets/plant.png",
      txt: "راه حل های شناسایی و کنترل علف های هرز",
    ),
    ExpertsSlider(
      img: "assets/message.png",
      txt: "پشتیبانی 24 ساعته و 7 روزه مشتری",
    ),
  ].obs;
}
