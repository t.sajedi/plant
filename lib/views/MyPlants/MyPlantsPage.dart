import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/views/MyPlants/Components/AppBar.dart';
import 'package:plant/views/MyPlants/Components/History/History.dart';
import 'package:plant/views/MyPlants/Components/Garden/MyGarden.dart';

// ignore: must_be_immutable
class MyPlantsPage extends StatelessWidget {
  PlantsPageCtrl plantsPageCtrl = Get.put(PlantsPageCtrl());
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Container(
        // color: Colors.red,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xff31a689), Color(0xff3ecb62)],
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            toolbarHeight: 50,
            // leadingWidth: 0,
            bottom: TabBar(
              // indicatorSize: TabBarIndicatorSize.,
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 2.0, color: Colors.yellow),
                insets: EdgeInsets.symmetric(horizontal: 80.0,vertical: 4),
              ),
              tabs: [
                appBar(context,title: "باغ من"),
                appBar(context,title: "تاریخچه"),

              ],
            ),
          ),
          body: TabBarView(
            children: [
              myGarden(context),
              history(context),
            ],
          ),
        ),
      ),
    );
  }
}
