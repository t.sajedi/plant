class PopularPlantsModel {
  final String imageURL;
  final String title;

  PopularPlantsModel({
    required this.imageURL,
    required this.title,
  });
}
