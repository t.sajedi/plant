import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/route_manager.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Books/BoocksPage.dart';

Widget title() {
  return Padding(
    // padding: EdgeInsets.symmetric(horizontal: horizmargin),
    padding: EdgeInsets.only(right: horizmargin * 1.8, left: horizmargin),
    child: Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          FontAwesomeIcons.chevronLeft,
          size: 16,
          color: Colors.green,
        ),
        SizedBox(
          width: 2,
        ),
        InkWell(
          onTap: () {
            Get.to(BooksPage());
          },
          child: Text(
            "بیشتر",
            style: TextStyle(
              fontSize: 24,
              // fontWeight: FontWeight.w700,
              color: Colors.green,
            ),
          ),
        ),
        Spacer(),
        Text(
          "کتاب های پرطرفدار",
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w700,
            color: Colors.green[800],
          ),
        ),
      ],
    ),
  );
}
