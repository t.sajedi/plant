import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Diagnose/DiagnosePageCtrlr.dart';
import 'package:plant/controllers/Static.dart';

Widget explorePlant(BuildContext context) {
  DiagnosePageCtrl diagnosePageCtrl = Get.find();
  return Container(
    height: 220 + verticmargin,
    width: MediaQuery.of(context).size.width,
    // color: Colors.red,
    child: Column(
      children: [
        SizedBox(height: verticmargin),
        Container(
          // height: 50,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: horizmargin),
          // color: Colors.black,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            textDirection: TextDirection.rtl,
            children: [
              Text(
                "کشف بیماری توسط گیاه",
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                  color: Colors.green[900],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0),
                child: Icon(
                  FontAwesomeIcons.search,
                  color: Colors.grey,
                  size: 32,
                ),
              )
            ],
          ),
        ),
        SizedBox(height: verticmargin),
        Expanded(
          child: Obx(() {
            return ListView.builder(
              reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: diagnosePageCtrl.diagnosePlants.length,
              padding: EdgeInsets.zero,
              itemBuilder: (context, index) {
                return Card(
                  margin: EdgeInsets.only(
                      right: horizmargin,
                      bottom: 4,
                      left: index == diagnosePageCtrl.diagnosePlants.length - 1
                          ? horizmargin / 2
                          : 0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)),
                  child: Container(
                    // height: 250,
                    width: MediaQuery.of(context).size.width * 0.38,
                    // color: Colors.red,
                    // decoration: BoxDecoration(
                    //   color: Colors.red,
                    //   borderRadius: BorderRadius.all(Radius.circular(12)),
                    // ),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          flex: 10,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8),
                                  topRight: Radius.circular(8)),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                  diagnosePageCtrl
                                      .diagnosePlants[index].imageURL,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Container(
                            // color: Colors.red,
                            margin: EdgeInsets.only(
                                right: horizmargin, top: horizmargin * 0.5),
                            child: Text(
                              diagnosePageCtrl.diagnosePlants[index].title,
                              textAlign: TextAlign.right,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: Colors.green[900],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }),
        ),
      ],
    ),
  );
}
