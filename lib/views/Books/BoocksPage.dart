import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/BooksCtrl.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Books/Components/appBar.dart';

import 'EachBook.dart';

class BooksPage extends StatelessWidget {
  // BooksCtrl booksCtrl = Get.put(BooksCtrl());
  BooksCtrl booksCtrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: SafeArea(
          child: Column(
            children: [
              appBar(
                context,
              ),
              Expanded(
                child: Container(
                  color: Colors.grey[200],
                  padding: EdgeInsets.only(top: verticmargin),
                  child: Obx(() {
                    return GridView.builder(
                      itemCount: booksCtrl.hotBooks.length,
                      padding: EdgeInsets.symmetric(horizontal: horizmargin),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: horizmargin,
                        mainAxisSpacing: verticmargin,
                        childAspectRatio: 5 / 10,
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: (){
                            booksCtrl.bookIndex.value = index;
                            booksCtrl.changePage(index);
                            log(booksCtrl.bookIndex.value.toString());
                            Get.to(EachBook());
                          },
                          child: Container(
                            // color: Colors.red,
                            child: Column(
                              children: [
                                Expanded(
                                  child: Container(
                                    // height: MediaQuery.of(context).size.height * 0.7,
                                    width: MediaQuery.of(context).size.width,
                                    // color: Colors.blue,
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)),
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        colorFilter: new ColorFilter.mode(
                                          Colors.black.withOpacity(0.2),
                                          BlendMode.darken,
                                        ),
                                        image: NetworkImage(
                                          booksCtrl.hotBooks[index].imageURL,
                                        ),
                                      ),
                                    ),
                                    child: Column(
                                      mainAxisAlignment: (index == 0)
                                          ? MainAxisAlignment.end
                                          : ((index % 3) == 0)
                                              ? MainAxisAlignment.end
                                              : MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.symmetric(
                                            horizontal: horizmargin,
                                            vertical: verticmargin,
                                          ),
                                          child: Text(
                                            // homePageCtrl.hotBooks[index].title,
                                            booksCtrl.hotBooks[index].title,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.w600,
                                            ),
                                            textAlign: TextAlign.right,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  margin: EdgeInsets.symmetric(
                                    horizontal: horizmargin,
                                    vertical: verticmargin,
                                  ),
                                  child: Text(
                                    // homePageCtrl.hotBooks[index].title,
                                    booksCtrl.hotBooks[index].title,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.right,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
