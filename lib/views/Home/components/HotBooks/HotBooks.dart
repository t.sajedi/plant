import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BooksCtrl.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Books/EachBook.dart';
import 'package:plant/views/Home/components/HotBooks/title.dart';
import 'package:plant/views/Home/components/HotBooks/shadow.dart';

Widget hotBooks(BuildContext context) {
  BooksCtrl booksCtrl = Get.find();
  return Container(
    height: 380,
    margin: EdgeInsets.only(top: verticmargin),
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: [
        title(),
        SizedBox(
          height: 6,
        ),
        Expanded(
          child: Obx(() {
            return ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: booksCtrl.hotBooks.length,
              scrollDirection: Axis.horizontal,
              reverse: true,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    log(index.toString());
                    booksCtrl.changePage(index);
                    Get.to(EachBook());
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: horizmargin,
                      right: index == 0 ? horizmargin : 0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        shadow(
                          h: 3,
                          w: MediaQuery.of(context).size.width * 0.34,
                          color: (Colors.grey[300]!),
                        ),
                        shadow(
                          h: 3,
                          w: MediaQuery.of(context).size.width * 0.36,
                          color: (Colors.grey[400]!),
                        ),
                        Container(
                          height: 250,
                          width: MediaQuery.of(context).size.width * 0.38,
                          // color: Colors.red,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                booksCtrl.hotBooks[index].imageURL,
                              ),
                              fit: BoxFit.cover,
                              colorFilter: new ColorFilter.mode(
                                  Colors.black.withOpacity(0.2),
                                  BlendMode.darken),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: (index == 0)
                                ? MainAxisAlignment.end
                                : ((index % 3) == 0)
                                    ? MainAxisAlignment.end
                                    : MainAxisAlignment.start,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(
                                  horizontal: horizmargin,
                                  vertical: verticmargin,
                                ),
                                child: cardTitle(
                                  title: booksCtrl.hotBooks[index].title,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.38,
                          padding: EdgeInsets.only(
                              left: 2, top: verticmargin, right: 3),
                          child: cardTitle(
                            color: (Colors.green[800])!,
                            title: booksCtrl.hotBooks[index].title,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }),
        ),
      ],
    ),
  );
}

Widget cardTitle({
  required String title,
  required Color color,
}) {
  return Text(
    // homePageCtrl.hotBooks[index].title,
    title,
    style: TextStyle(
      color: color,
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
    ),
    textAlign: TextAlign.right,
    maxLines: 2,
    overflow: TextOverflow.ellipsis,
  );
}
