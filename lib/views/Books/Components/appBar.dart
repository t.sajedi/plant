import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/route_manager.dart';
import 'package:plant/controllers/Static.dart';

Widget appBar(BuildContext context) {
  return Container(
    height: 65,
    width: MediaQuery.of(context).size.width,
    color: Colors.white,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      textDirection: TextDirection.rtl,
      children: [
        IconButton(
          icon: Icon(
            FontAwesomeIcons.arrowRight,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        SizedBox(width: horizmargin,),
        Padding(
          padding: const EdgeInsets.all(4),
          child: Text(
            "کتاب ها",
            textAlign: TextAlign.start,
            style: TextStyle(
              fontSize: 28,
            ),
          ),
        ),
        Spacer()
      ],
    ),
  );
}
