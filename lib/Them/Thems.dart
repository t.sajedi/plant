import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ColorsCtrl extends GetxController{
  final backcolor = Color(0xfffff7f7).obs;
  final errorColor = Color(0xff600000).obs;
  final hilightColor = Color(0xffedd2d2).obs;
  final fontColor = Color(0xff454545).obs;
  final warningColor = Color(0xffffd147).obs;
  final bottomBarColor = Colors.white.obs;
  final bottomBarActiveColor = Colors.teal.obs;
}


