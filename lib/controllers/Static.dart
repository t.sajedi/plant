import 'package:get/get.dart';

  final horizmargin = 12.0;
  final verticmargin = 16.0;
class StaticCtrl extends GetxController {
  final height = 0.0.obs;
  final width = 0.0.obs;

  updatewidth({double? newvalue}) {
    width.value = newvalue!;
  }

  updateheight({double? newvalue}) {
    height.value = newvalue!;
  }

  double? getwidth() {
    return width.value;
  }

  double? getheight() {
    return height.value;
  }
}
