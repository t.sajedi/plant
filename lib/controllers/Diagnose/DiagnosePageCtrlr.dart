import 'package:get/get.dart';
import 'package:plant/models/Diagnose/DiagnosePart.dart';
import 'package:plant/models/Diagnose/DiagnosePlant.dart';

class DiagnosePageCtrl extends GetxController {
  List diagnosePlants = [
    DiagnosePlant(
      imageURL:
          "https://ae01.alicdn.com/kf/HTB1tb3Le8Cw3KVjSZR0q6zcUpXaW/Big-size-artificial-flowers-china-Rose-flower-head-wedding-flowers-wall-decoration-corsage-headdress-Accessories-13CM.jpg_640x640.jpg",
      title: "رز چینی",
    ),
    DiagnosePlant(
      imageURL:
          "https://trulyexperiences.com/blog/wp-content/uploads/2021/02/Common-Orange-Daylily-420x329.jpg",
      title: "پرتقال روزانه",
    ),
    DiagnosePlant(
      imageURL:
          "https://images.homedepot-static.com/productImages/48a8ad6a-9791-4eeb-98fe-a88edfc8b687/svn/ornamental-trees-crmsio01g-64_1000.jpg",
      title: "کرپ میرتل",
    ),
    DiagnosePlant(
      imageURL:
          "https://ae01.alicdn.com/kf/HTB1tb3Le8Cw3KVjSZR0q6zcUpXaW/Big-size-artificial-flowers-china-Rose-flower-head-wedding-flowers-wall-decoration-corsage-headdress-Accessories-13CM.jpg_640x640.jpg",
      title: "رز چینی",
    ),
    DiagnosePlant(
      imageURL:
          "https://trulyexperiences.com/blog/wp-content/uploads/2021/02/Common-Orange-Daylily-420x329.jpg",
      title: "پرتقال روزانه",
    ),
    DiagnosePlant(
      imageURL:
          "https://images.homedepot-static.com/productImages/48a8ad6a-9791-4eeb-98fe-a88edfc8b687/svn/ornamental-trees-crmsio01g-64_1000.jpg",
      title: "کرپ میرتل",
    ),
  ].obs;

  List diagnoseParts = [
    DiagnosePart(
      imageURL:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSL3lQ7OzplIP0O3wzUK2jM06f6jAA1hSt0Ig&usqp=CAU",
      titlew: "بیماری",
      titley: "کل گیاه",
    ),
    // DiagnosePart(titley: titley, imageURL: imageURL, titlew: title)
    DiagnosePart(
        imageURL:
            "https://www.thespruce.com/thmb/Y9VshnwBdJoxBNzxGOUgF278W0o=/735x0/treating-brown-spots-on-leaves-5076039-01-b05c9ed7a44648adbee4c7d4bc2b2bab.jpg",
        titlew: "بیماری",
        titley: "برگ ها"),
    DiagnosePart(
      imageURL:
          "https://agfax.com/wp-content/uploads/Stem-Canker-1-1.jpg",
      titlew: "بیماری",
      titley: "ساقه",
    ),
    DiagnosePart(
      imageURL:
          "https://www.innovationnewsnetwork.com/wp-content/uploads/2020/01/fungal-flower-protection-696x392.jpg",
      titlew: "بیماری",
      titley: "گل ها",
    ),
    DiagnosePart(
      imageURL:
          "https://www.kenmuir.co.uk/image/data/icons/Pest%20and%20Diseases/GREY-MOULD-DIS23.jpg",
      titlew: "بیماری",
      titley: "میوه ها",
    ),
    DiagnosePart(
        imageURL:
            "https://www.uaex.edu/farm-ranch/pest-management/plant-disease/images/Faske%20Aspergillus%20Crown%20Rot.jpg",
        titlew: "بیماری",
        titley: "ریشه ها"),
    DiagnosePart(
        imageURL:
            "https://www.gardeners.com/globalassets/articles/gardening/hero_thumbnail/5168-aphids.jpg?\$staticlink\$",
        titlew: "بیماری توسط",
        titley: "آفت ها"),
  ].obs;
}
