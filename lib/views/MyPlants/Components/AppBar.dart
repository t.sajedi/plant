import 'package:flutter/material.dart';

Widget appBar(BuildContext context, {required String title}) {
  return Container(
    margin: EdgeInsets.only(bottom: 4),
    child: Text(
      title,
      style: TextStyle(
        fontSize: 24,
        // color: Colors.white,
      ),
    ),
  );
}