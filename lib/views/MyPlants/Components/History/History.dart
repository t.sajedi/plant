import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/MyPlants/Components/Garden/ListOfCard.dart';

Widget history(BuildContext context) {
  PlantsPageCtrl plantsPageCtrl = Get.put(PlantsPageCtrl());

  return Container(
    color: Colors.white,
    child: Obx(() {
      return plantsPageCtrl.plants.isEmpty
          ? Container(
              color: Colors.grey.withOpacity(0.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.delete_outline_rounded,
                    size: 52,
                    color: Colors.grey,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "هیچ گیاهی شناسایی نشده است",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 26,
                      // fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: 4),
                  Text(
                    "از گیاهان اطراف خود عکس بگیرید تا بدانید آنها چه هستند",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24,
                      // fontWeight: FontWeight.w200,
                      color: Colors.grey[600],
                    ),
                  ),
                  // SizedBox(height: verticmargin),
                ],
              ),
            )
          : listOfCars(context);
    }),
  );
}
