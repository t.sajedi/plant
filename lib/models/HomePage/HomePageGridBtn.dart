import 'package:flutter/widgets.dart';

class HomePageGridBTN {
  final String title;
  final IconData icon;
  final Color iconColor;
  final Widget pageIndex;

  HomePageGridBTN({
    required this.title,
    required this.icon,
    required this.pageIndex,
    required this.iconColor,
  });
}
