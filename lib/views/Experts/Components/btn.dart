import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:plant/controllers/Static.dart';

Widget continueBtn(BuildContext context) {
  return InkWell(
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    onTap: () {
      log("continuebtn pressed");
    },
    child: Container(
      height: 70,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: horizmargin),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [Color(0xff31a689), Color(0xff3ecb62)],
        ),
        borderRadius: BorderRadius.all(Radius.circular(70 / 2)),
      ),
      alignment: Alignment.center,
      child: Text(
        "ادامه دادن",
        textAlign: TextAlign.right,
        style: TextStyle(
          fontSize: 28,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
      ),
    ),
  );
}
