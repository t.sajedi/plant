import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/Static.dart';

Widget service({
  required BuildContext context,
  required String title,
  required String cap,
  required IconData icon,
  required String image,
}) {
  return Container(
    height: 120,
    width: MediaQuery.of(context).size.width,
    color: Colors.white,
    margin:
        EdgeInsets.symmetric(horizontal: horizmargin, vertical: verticmargin),
    child: Row(
      textDirection: TextDirection.rtl,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Icon(
        //   icon,
        //   color: Colors.brown,
        //   size: 42,
        // ),
        Container(
          height: 58,
          width: 58,
          // color: Colors.red,
          child: Image(image: AssetImage(image)),
        ),
        SizedBox(width: horizmargin),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                title,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: TextStyle(
                  height: 1,
                  color: Colors.green[900],
                  fontSize: 22,
                ),
              ),
              SizedBox(
                height: verticmargin * 0.5,
              ),
              Text(
                cap,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: Colors.green[600],
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
        SizedBox(width: horizmargin),
        Icon(
          Icons.camera_alt_outlined,
          size: 38,
          color: Colors.green,
        ),
      ],
    ),
  );
}
