import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Books/BoocksPage.dart';
import 'package:plant/views/More/Components/AppBar.dart';
import 'package:plant/views/More/Components/Service.dart';

class MorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color(0xff31a689), Color(0xff3ecb62)],
          ),
        ),
        child: SafeArea(
          child: Column(
            children: [
              appbar(context),
              Container(height: verticmargin, color: Colors.grey[200]),
              Expanded(
                child: Container(
                  color: Colors.grey[200],
                  child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: horizmargin),
                    scrollDirection: Axis.vertical,
                    children: [
                      SizedBox(height: horizmargin),
                      InkWell(
                        onTap: () {
                          Get.to(BooksPage());
                        },
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          child: Container(
                            height: 86,
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(
                                horizontal: horizmargin,
                                vertical: verticmargin),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              textDirection: TextDirection.rtl,
                              children: [
                                Icon(
                                  FontAwesomeIcons.book,
                                  color: Colors.green,
                                  size: 36,
                                ),
                                SizedBox(width: horizmargin * 1.6),
                                Text(
                                  "کتاب ها",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    height: 1,
                                    fontSize: 28,
                                    color: Colors.green[900],
                                  ),
                                ),
                                Spacer(),
                                Icon(
                                  FontAwesomeIcons.chevronLeft,
                                  color: Colors.grey,
                                  size: 16,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: verticmargin * 2),
                      Container(
                        margin: EdgeInsets.only(right: horizmargin),
                        child: Text(
                          "امکانات بیشتر",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 24,
                          ),
                        ),
                      ),
                      Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        child: Column(
                          children: [
                            service(
                              context: context,
                              cap: "آیا در باغ من علف های هرز وجود دارد؟" +
                                  "\n" +
                                  "آنها را از گیاهان من تشخیص داده و متمایز کنم",
                              title: "شناسایی علفهای هرز",
                              icon: Icons.ac_unit_outlined,
                              image: "assets/veg.png"
                            ),
                            Container(
                              height: 0.6,
                              color: Colors.green[100],
                              margin: EdgeInsets.only(right: 65),
                            ),
                            service(
                              context: context,
                              cap: "آیا در باغ من علف های هرز وجود دارد؟" +
                                  "\n" +
                                  "آنها را از گیاهان من تشخیص داده و متمایز کنم",
                              title: "شناسایی علفهای هرز",
                              icon: Icons.ac_unit_outlined,
                              image: "assets/radio.png"
                            ),
                            Container(
                              height: 0.6,
                              color: Colors.green[100],
                              margin: EdgeInsets.only(right: 65),
                            ),
                            service(
                              context: context,
                              cap: "آیا در باغ من علف های هرز وجود دارد؟" +
                                  "\n" +
                                  "آنها را از گیاهان من تشخیص داده و متمایز کنم",
                              title: "شناسایی علفهای هرز",
                              icon: Icons.ac_unit_outlined,
                              image: "assets/blue.png"
                            ),
                            Container(
                              height: 0.6,
                              color: Colors.green[100],
                              margin: EdgeInsets.only(right: 65),
                            ),
                            service(
                              context: context,
                              cap: "آیا در باغ من علف های هرز وجود دارد؟" +
                                  "\n" +
                                  "آنها را از گیاهان من تشخیص داده و متمایز کنم",
                              title: "شناسایی علفهای هرز",
                              icon: Icons.ac_unit_outlined,
                              image: "assets/bird.png"
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 40),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
