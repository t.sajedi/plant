import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/views/Home/components/PopularPlants/PopularPlants.dart';
import 'package:plant/views/Home/components/gridView.dart';
import '/controllers/Static.dart';

import 'components/GetStarted/GetStarted.dart';
import 'components/HotBooks/HotBooks.dart';
import 'components/searchbtn.dart';

// ignore: must_be_immutable
class HomePage extends StatelessWidget {
  HomePageCtrl homePageCtrl = Get.find();
  TextEditingController textController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.red,
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            // color: Colors.red,
            image: DecorationImage(
              image: AssetImage('assets/back.jpg'),
              fit: BoxFit.fill,
            ),
          ),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                searchBtn(),
                SizedBox(
                  height: verticmargin,
                ),
                Expanded(
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    padding: EdgeInsets.zero,
                    children: [
                      centerGridView(ctx: context),
                      hotBooks(context),
                      getStartded(context),
                      popularPlants(context),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
