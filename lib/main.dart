import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/BottomBarCtrl.dart';
import 'package:plant/views/BottomBar/BottomBar.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'Them/Thems.dart';

void main() {
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark),
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // final StaticCtrl staticCtrl = Get.put(StaticCtrl());
  // final ColorsCtrl colorsCtrl = Get.put(ColorsCtrl());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      builder: (context, widget) {
        // Get.lazyPut(()=>BottomBarCtrl());
        // final BottomBarCtrl bottomBarCtrl = Get.put(BottomBarCtrl());
        // final ColorsCtrl colorCtrl = Get.put(ColorsCtrl());
        return ResponsiveWrapper.builder(
          BouncingScrollWrapper.builder(context, widget!),
          maxWidth: 1200,
          minWidth: 400,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(450, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
        );
      },
      home: BottombarPage(),
    );
  }
}
