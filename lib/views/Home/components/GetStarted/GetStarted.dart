import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/HomePageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Home/components/GetStarted/title.dart';

Widget getStartded(BuildContext ctx) {
  HomePageCtrl homePageCtrl = Get.find();
  return Container(
    height: 300,
    margin: EdgeInsets.only(top: verticmargin),
    width: MediaQuery.of(ctx).size.width,
    // color: Colors.blue,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        title(),
        SizedBox(height: verticmargin * 0.4,),
        Expanded(
          child: Obx(() {
            return ListView.builder(
              reverse: true,
              scrollDirection: Axis.horizontal,
              itemCount: homePageCtrl.getStarteds.length,
              padding: EdgeInsets.zero,
              itemBuilder: (ctx, index) {
                return Container(
                  width: MediaQuery.of(ctx).size.width * 0.8,
                  margin: EdgeInsets.only(
                    left: horizmargin,
                    right: index == 0 ? horizmargin : 0,
                  ),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.2), BlendMode.darken),
                      image: NetworkImage(
                        homePageCtrl.getStarteds[index].imageURL,
                      ),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      cardTitle(
                        title: homePageCtrl.getStarteds[index].title,
                        color: Colors.white,
                      ),
                    ],
                  ),
                );
              },
            );
          }),
        ),
      ],
    ),
  );
}

Widget cardTitle({
  required String title,
  required Color color,
}) {
  return Container(
    margin:
        EdgeInsets.symmetric(horizontal: horizmargin, vertical: verticmargin),
    child: Text(
      // homePageCtrl.hotBooks[index].title,
      title,
      style: TextStyle(
        color: color,
        fontSize: 22.0,
        fontWeight: FontWeight.w600,
      ),
      textAlign: TextAlign.right,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    ),
  );
}
