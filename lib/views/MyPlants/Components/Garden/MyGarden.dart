import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/PlantsPageCtrl.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Camera/CameraAlert.dart';

import 'ListOfCard.dart';

Widget myGarden(BuildContext context) {
  PlantsPageCtrl plantsPageCtrl = Get.find();

  return Container(
      color: Colors.white,
      child: Obx(() {
        return plantsPageCtrl.plants.isEmpty
            ? Container(
                color: Colors.grey.withOpacity(0.1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.delete_outline_rounded,
                      size: 52,
                      color: Colors.grey,
                    ),
                    SizedBox(height: 8),
                    Text(
                      "هیچ گیاهی در باغ شما وجود ندارد",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 26,
                        // fontWeight: FontWeight.w600,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 4),
                    Text(
                      "اولین گیاه خود را برای مراقبت از آن اضافه کنید",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        // fontWeight: FontWeight.w200,
                        color: Colors.grey[600],
                      ),
                    ),
                    Text(
                      plantsPageCtrl.plants.length.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        // fontWeight: FontWeight.w200,
                        color: Colors.grey[600],
                      ),
                    ),
                    SizedBox(height: verticmargin),
                    InkWell(
                      onTap: () {
                        showImageAlert(context);
                      },
                      child: Container(
                        height: 46,
                        width: 46,
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: Center(
                          child: Icon(
                            Icons.plus_one,
                            color: Colors.green,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : listOfCars(context);
      }));
}
