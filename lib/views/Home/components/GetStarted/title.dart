import 'package:flutter/material.dart';

Widget title() {
  return Text(
    "شروع به کار",
    style: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w700,
      color: Colors.green[800],
    ),
  );
}
