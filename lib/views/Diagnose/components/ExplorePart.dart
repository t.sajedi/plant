import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Diagnose/DiagnosePageCtrlr.dart';
import 'package:plant/controllers/Static.dart';

Widget explorePart(BuildContext context) {
  DiagnosePageCtrl diagnosePageCtrl = Get.find();
  // log("hi");
  return Column(
    children: [
      Container(
        // height: 50,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
            horizontal: horizmargin, vertical: verticmargin),
        // color: Colors.black,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          textDirection: TextDirection.rtl,
          children: [
            Text(
              "کاوش بیماریها بر اساس بخش گیاه",
              textAlign: TextAlign.end,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 24,
                color: Colors.green[900],
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(left: 0),
            //   child: Icon(
            //     FontAwesomeIcons.search,
            //     color: Colors.grey,
            //     size: 32,
            //   ),
            // )
          ],
        ),
      ),
      // SizedBox(height: verticmargin),
      Container(
        height: 520,
        // color: Colors.black,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: horizmargin),
        child: Obx(() {
          return GridView.builder(
            itemCount: diagnosePageCtrl.diagnoseParts.length,
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: horizmargin,
              mainAxisSpacing: verticmargin,
              childAspectRatio: 10 / 6.5,
            ),
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  log("hi");
                },
                child: Container(
                  // color: Colors.red,
                  padding: EdgeInsets.only(
                      right: horizmargin * 2, top: verticmargin),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.2), BlendMode.darken),
                      image: NetworkImage(
                        diagnosePageCtrl.diagnoseParts[index].imageURL,
                      ),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      texts(
                        message: diagnosePageCtrl.diagnoseParts[index].titlew,
                        color: Colors.white,
                        size: 24,
                        weight: FontWeight.w400,
                      ),
                      texts(
                        message: diagnosePageCtrl.diagnoseParts[index].titley,
                        color: Colors.yellow,
                        size: 32,
                        weight: FontWeight.w600,
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }),
      ),
    ],
  );
}

Widget texts({
  required String message,
  required Color color,
  required double size,
  required FontWeight weight,
}) {
  return Text(
    message,
    textAlign: TextAlign.end,
    style: TextStyle(
      fontSize: size,
      fontWeight: weight,
      color: color,
    ),
  );
}
