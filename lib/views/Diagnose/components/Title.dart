import 'package:flutter/material.dart';
import 'package:plant/controllers/Static.dart';

Widget titleWithIcon({
  required BuildContext context,
  required String title,
  required IconData icon,
}) {
  return Container(
    // height: 50,
    width: MediaQuery.of(context).size.width,
    margin: EdgeInsets.symmetric(horizontal: horizmargin),
    // color: Colors.black,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      textDirection: TextDirection.rtl,
      children: [
        Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 24,
            color: Colors.white,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 0),
          child: Icon(
            icon,
            color: Colors.white,
            size: 32,
          ),
        )
      ],
    ),
  );
}
