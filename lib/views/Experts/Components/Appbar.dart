import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:images_picker/images_picker.dart';
import 'package:plant/controllers/Static.dart';

Widget appBar(BuildContext context) {
  return SafeArea(
    child: Container(
      height: 65,
      width: MediaQuery.of(context).size.width,
      // color: Colors.red,
      padding: EdgeInsets.symmetric(horizontal: horizmargin),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: (){
              // log("moooooooooooooooooz");
              Get.back();
            },
            child: Icon(
              Icons.cancel_outlined,
              color: Colors.grey[700],
            ),
          ),
          Text(
            "بازیابی",
            style: TextStyle(
              fontSize: 26,
              color: Colors.grey[700],
            ),
          ),
        ],
      ),
    ),
  );
}
