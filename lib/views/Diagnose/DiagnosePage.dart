import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:plant/controllers/Diagnose/DiagnosePageCtrlr.dart';
import 'package:plant/controllers/Static.dart';
import 'package:plant/views/Diagnose/components/AskExperts.dart';
import 'package:plant/views/Diagnose/components/AutoDiagnose.dart';
import 'package:plant/views/Diagnose/components/ExplorePart.dart';
import 'package:plant/views/Diagnose/components/ExplorePlant.dart';
import 'package:plant/views/Diagnose/components/Title.dart';

// ignore: must_be_immutable
class DiagnosePage extends StatelessWidget {
  // DiagnosePageCtrl diagnosePageCtrl = Get.put(DiagnosePageCtrl());
  final DiagnosePageCtrl diagnosePageCtrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.blue,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/back.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              titleWithIcon(
                context: context,
                title: "تشخیص",
                icon: FontAwesomeIcons.clock,
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    SizedBox(height: verticmargin),
                    autoDiagnose(context),
                    explorePlant(context),
                    explorePart(context),
                    askExperts(context),
                    SizedBox(height: 30,)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
